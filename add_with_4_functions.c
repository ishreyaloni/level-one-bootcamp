//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float input()
{
    float a; 
    printf("Enter the number\n");
    scanf("%f",&a);
    return a;
}

float funcsum(float a, float b)
{
    float sum=0;
    sum = a+b;
    return sum;
}

void output(float a, float b, float c)
{
    printf("Sum of %f + %f is %f\n",a,b,c);
}

float main()
{
    float x,y,z;
    x=input();
    y=input();
    z=funcsum(x,y);
    output(x,y,z);
    return 0;
}

