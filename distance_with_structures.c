//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct point
{
float x;
float y;
};
typedef struct point Point;
Point input()
{
Point p;
printf("Enter the value of abscissa");
scanf("%f",&p.x);
printf("Enter the value of ordinate");
scanf("%f",&p.y);
return p;
}
float compute(Point p1,Point p2)
{
float distance;
distance = sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
return distance;
}
void output(float distance)
{
printf("The distance between given points is %f",distance);
}
int main()
{
float dist;
Point p1,p2;
p1=input();
p2=input();
dist = compute(p1,p2);
output(dist);
return 0;
}

