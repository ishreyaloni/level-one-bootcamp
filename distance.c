//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float coordinates()
{
float a;
scanf("%f",&a);
return a;
}
float distance(float x1,float x2,float y1,float y2)
{
float x,y,dist;
x= x2-x1;
y= y2-y1;
dist = (sqrt(pow(x,2) + pow(y,2)));
return dist;
}
void output(float dist)
{
printf("The distance between the given points is %f",dist);
}
float main()
{
float x1,x2,y1,y2,d;
printf("Enter the value of x1:");
x1 = coordinates();
printf("Enter the value of x2:");
x2 = coordinates();
printf("Enter the value of y1:");
y1 = coordinates();
printf("Enter the value of y2:");
y2 = coordinates();

d = distance(x1,x2,y1,y2);
output(d);
return 0;
}

